import SessionManager from 'session-manager';
import angular from 'angular';
import PartnerSaleInvoiceServiceSdk,{AddPartnerSaleInvoiceReq} from 'partner-sale-invoice-service-sdk';
import Iso3166Sdk from 'iso-3166-sdk';

export default class Controller {

    _sessionManager:SessionManager;

    _$modalInstance;

    _$q;

    _partnerSaleInvoiceServiceSdk:PartnerSaleInvoiceServiceSdk;

    _countryList;

    _addPartnerSaleInvoiceReqData;

    constructor(sessionManager:SessionManager,
                $modalInstance,
                $q,
                partnerSaleInvoiceServiceSdk:PartnerSaleInvoiceServiceSdk,
                iso3166Sdk:Iso3166Sdk,
                initialAddPartnerSaleInvoiceReqData) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!$modalInstance) {
            throw new TypeError('$modalInstance required');
        }
        this._$modalInstance = $modalInstance;

        if (!$q) {
            throw new TypeError('$q required');
        }
        this._$q = $q;

        if (!partnerSaleInvoiceServiceSdk) {
            throw new TypeError('partnerSaleInvoiceServiceSdk required');
        }
        this._partnerSaleInvoiceServiceSdk = partnerSaleInvoiceServiceSdk;

        if (!iso3166Sdk) {
            throw new TypeError('iso3166Sdk required');
        }

        $q(resolve =>
            resolve(iso3166Sdk.listCountries())
        ).then(countryList => {
            this._countryList = countryList;
        });

        // handle provided initialAddPartnerSaleInvoiceReqData
        if (initialAddPartnerSaleInvoiceReqData) {

            this._addPartnerSaleInvoiceReqData = angular.copy(initialAddPartnerSaleInvoiceReqData) || {};

        }
    }

    get addPartnerSaleInvoiceReqData() {
        return this._addPartnerSaleInvoiceReqData;
    }

    handleFileSelection(selectedFiles):void {

        if (selectedFiles && selectedFiles[0]) {
            this._addPartnerSaleInvoiceReqData.file = selectedFiles[0];
        }
        else {
            this._addPartnerSaleInvoiceReqData.file = null;
        }

    }

    cancel() {
        this._$modalInstance.dismiss();
    }

    saveAndClose(form) {
        if (form.$valid) {

            this._sessionManager
                .getAccessToken()
                .then(accessToken =>
                    this._partnerSaleInvoiceServiceSdk
                        .addPartnerSaleInvoice(
                            new AddPartnerSaleInvoiceReq(
                                this._addPartnerSaleInvoiceReqData.number,
                                this._addPartnerSaleInvoiceReqData.file,
                                this._addPartnerSaleInvoiceReqData.partnerSaleRegistrationId
                            ),
                            accessToken
                        )
                )
                .then(partnerSaleInvoiceId =>
                    this._$modalInstance.close(partnerSaleInvoiceId)
                );

        }
        else {
            form.$setSubmitted();
        }
    }
};

Controller.$inject = [
    'sessionManager',
    '$modalInstance',
    '$q',
    'partnerSaleInvoiceServiceSdk',
    'iso3166Sdk',
    'initialAddPartnerSaleInvoiceReqData'
];