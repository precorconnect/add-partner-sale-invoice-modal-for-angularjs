import angular from 'angular';
import AddPartnerSaleInvoiceModal from './addPartnerSaleInvoiceModal';
import errorMessagesTemplate from './error-messages.html!text';
import 'angular-bootstrap';
import 'angular-messages';
import 'angular-file-upload';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'

angular
    .module(
        'addPartnerSaleInvoiceModal.module',
        [
            'ui.bootstrap',
            'ngMessages',
            'ngFileUpload'
        ]
    )
    .service(
        'addPartnerSaleInvoiceModal',
        [
            '$modal',
            AddPartnerSaleInvoiceModal
        ]
    )
    .run(
        [
            '$templateCache',
            $templateCache => {
                $templateCache.put(
                    'add-partner-sale-invoice-modal/error-messages.html',
                    errorMessagesTemplate
                );
            }
        ]
    );