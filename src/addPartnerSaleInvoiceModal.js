import Controller from './controller';
import './default.css!css';
import template from './index.html!text';

export default class AddPartnerSaleInvoiceModal {

    _$modal;

    constructor($modal) {

        if (!$modal) {
            throw new TypeError('$modal required');
        }
        this._$modal = $modal;

    }

    /**
     * Shows the add facility contact modal
     * @param {AddPartnerSaleInvoiceReq} initialAddPartnerSaleInvoiceReqData
     * optional custom initialization data of the add partner sale invoice request
     * @returns {Promise<string>} partner sale invoice id
     */
    show(initialAddPartnerSaleInvoiceReqData):Promise<string> {

        let modalInstance = this._$modal.open({
            controller: Controller,
            controllerAs: 'controller',
            template: template,
            backdrop: 'static',
            resolve: {
                initialAddPartnerSaleInvoiceReqData: () => initialAddPartnerSaleInvoiceReqData
            }
        });

        return modalInstance.result;
    }
}

AddPartnerSaleInvoiceModal.$inject = [
    '$modal'
];