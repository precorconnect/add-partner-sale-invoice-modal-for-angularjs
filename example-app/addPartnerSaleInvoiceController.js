import SessionManager from 'session-manager';
import PartnerSaleInvoiceServiceSdk from 'partner-sale-invoice-service-sdk';

export default class AddPartnerSaleInvoiceController {

    _sessionManager:SessionManager;

    _addPartnerSaleInvoiceModal;

    _partnerSaleInvoiceServiceSdk:PartnerSaleInvoiceServiceSdk;

    _initialAddPartnerSaleInvoiceReqData = {};

    _partnerSaleInvoice;

    constructor(sessionManager:SessionManager,
                addPartnerSaleInvoiceModal,
                partnerSaleInvoiceServiceSdk:PartnerSaleInvoiceServiceSdk) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;
        
        if (!addPartnerSaleInvoiceModal) {
            throw new TypeError('addPartnerSaleInvoiceModal required');
        }
        this._addPartnerSaleInvoiceModal = addPartnerSaleInvoiceModal;

        if (!partnerSaleInvoiceServiceSdk) {
            throw new TypeError('partnerSaleInvoiceServiceSdk required');
        }
        this._partnerSaleInvoiceServiceSdk = partnerSaleInvoiceServiceSdk;
    }

    get initialAddPartnerSaleInvoiceReqData() {
        return this._initialAddPartnerSaleInvoiceReqData;
    }

    get partnerSaleInvoice() {
        return this._partnerSaleInvoice;
    }

    showAddPartnerSaleInvoiceModal() {

        this._addPartnerSaleInvoiceModal
            .show(this._initialAddPartnerSaleInvoiceReqData)
            .then(partnerSaleInvoiceId => {
                this._partnerSaleInvoice = {id: partnerSaleInvoiceId};
            })
            .then(() => this._sessionManager.getAccessToken())
            .then(accessToken =>
                this._partnerSaleInvoiceServiceSdk
                    .getPartnerSaleInvoiceWithId(
                        this._partnerSaleInvoice.id,
                        accessToken
                    )
            )
            .then(partnerSaleInvoice => {
                    this._partnerSaleInvoice = partnerSaleInvoice;
                }
            );

    }

}

AddPartnerSaleInvoiceController.$inject = [
    'sessionManager',
    'addPartnerSaleInvoiceModal',
    'partnerSaleInvoiceServiceSdk'
];
