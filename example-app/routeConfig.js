import addPartnerSaleInvoiceTemplate from './add-partner-sale-invoice.html!text';
import AddPartnerSaleInvoiceController from './addPartnerSaleInvoiceController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: addPartnerSaleInvoiceTemplate,
                    controller: AddPartnerSaleInvoiceController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];