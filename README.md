## Description
Add partner sale invoice modal for AngularJS.

## Example
refer to the [example app](example) for working example code.

## Setup

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/add-partner-sale-invoice-modal-for-angularjs
``` 

**import & wire up**
```js
import 'add-partner-sale-invoice-modal-for-angularjs';

angular.module(
            "app",
            ["add-partner-sale-invoice-modal.module"]
        )
        // ensure dependencies available in container
        .constant(
            'sessionManager', 
            sessionManager
            /*see https://bitbucket.org/precorconnect/session-manager-for-browsers*/
        )
        .constant(
            'partnerSaleInvoiceServiceSdk',
            partnerSaleInvoiceServiceSdk
            /*see https://bitbucket.org/precorconnect/partner-sale-invoice-service-sdk-for-javascript*/
        )
        .constant(
            'iso3166Sdk',
            iso3166Sdk
            /*see https://bitbucket.org/precorconnect/iso-3166-sdk-for-javascript*/
        );
```